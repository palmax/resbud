<?php


namespace App\Http\Controllers;


use App\Http\Requests\PageRequest;
use App\Models\NavItem;

class NavItemController extends Controller
{
    public function partial($view) {
        $view->items = NavItem::with(['navItems'])
            ->activeAndLocale()
            ->where('nav_item_id', '=', null)
            ->where('nav_name', '=', $view->name)
            ->orderByDesc('position')
            ->get();
    }
}
