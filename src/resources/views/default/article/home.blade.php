@foreach($items as $item)
    <div class="col-xl-4 col-md-6 mb-30">
        <div class="services-item">
            <div class="services-wrap">
                <div class="services-image">
                    <img style="min-height: 376px; object-fit: cover;" src="{{renderImage($item->galleryCover(), 'fit')}}" alt="{{$item->title}}">
                </div>
                <div class="services-content">
                    <div class="service-inner">
                        <div class="icon-top">
                            <img src="{{ asset('images/icon/' . $item->text . '.png') }}" alt="{{$item->title}}">

                        </div>
                        <div class="services-titles">
                            <h3 class="title" style="color: #fff">{{$item->title}}</h3>
                        </div>
                        <p class="services-txt">{!! $item->lead !!}</p>
                        <div class="services-btn">
                            <a class="btn-text" href="{{route('offer.index')}}">Zobacz ofertę</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach
