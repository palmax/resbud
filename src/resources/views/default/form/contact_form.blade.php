
<form id="contactForm" method="POST">
    <fieldset>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 mb-30 form-group">
                <input class="from-control" type="text" id="name" name="name" placeholder="Imię" required="true">
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 mb-30 form-group">
                <input class="from-control" type="text" id="email" name="email" placeholder="Email" required="true">
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-lg-12 col-sm-6 mb-30 form-group">
                <input class="from-control" type="text" id="phone" name="phone" placeholder="Numer telefonu" required="">
                <div class="invalid-feedback"></div>
            </div>
            <div class="col-lg-12 mb-30 form-group">
                <textarea class="from-control" id="message" name="message" placeholder="Wiadomość" required="true"></textarea>
                <div class="invalid-feedback"></div>
            </div>
        </div>



    <div class="form-group">
        <div class="form-check">
            <input id="rule" type="checkbox" name="rule" placeholder="Rule" class="form-check-input">
            <label for="rule" class="form-check-label" style="color: #fff">{!! getConstField('contact_form_rule') !!}</label>
            <div class="invalid-feedback"></div>
        </div>
    </div>

    <div class="form-group">
        <div class="g-recaptcha" data-sitekey="{{$siteKey}}"></div>
        <div class="invalid-feedback"></div>
    </div>
    <div class="btn-part">
        <div class="form-group mb-0">
            <input class="readon more submit sub-con" type="submit" value="Wyślij">
        </div>
    </div>
    <div id="alert" class="alert"></div>
    </fieldset>
</form>



@push('scripts.body.bottom')
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

    <script>
        document.getElementById('contactForm').addEventListener('submit', e => {
            e.preventDefault();
            submitForm(e.target);
        })
    </script>
@endpush
