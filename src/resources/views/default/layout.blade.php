<!doctype html>
<html lang="{{app()->getLocale()}}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    {!! SEOMeta::generate() !!}

    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('image/favicon/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('image/favicon/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('image/favicon/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('image/favicon/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('image/favicon/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('image/favicon/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('image/favicon/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('image/favicon/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('image/favicon/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('image/favicon/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('image/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('image/favicon/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('image/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('image/favicon//manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">


    <link href="{{asset('css/vendors/bootstrap.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link href="{{asset('css/main.css')}}?version=1" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simplelightbox/2.10.3/simple-lightbox.min.css" integrity="sha512-Ne9/ZPNVK3w3pBBX6xE86bNG295dJl4CHttrCp3WmxO+8NQ2Vn8FltNr6UsysA1vm7NE6hfCszbXe3D6FUNFsA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- Bootstrap v4.4.1 css -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/uicons-regular-rounded.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/flaticon.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/slick.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/off-canvas.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('css/rsmenu-main.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/rs-spacing.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/responsive.css')}}">

    <script>
        const BASE_URL = '{{url()->to('/')}}/';
        const CSRF_TOKEN = '{{csrf_token()}}';
        const SITE_LANG = '{{app()->getLocale()}}';
    </script>

    @stack('scrips.head.bottom')
</head>
<body class="defult-home">

<div class="offwrap"></div>

<div id="pre-load">
    <div id="loader" class="loader">
        <div class="loader-container">
            <div class="loader-icon"><img src="{{asset('/image/logo/fav.png')}}"
                                          alt="RESBUD"></div>
        </div>
    </div>
</div>
<!--Preloader area end here-->

<!-- Main content Start -->
<div class="main-content">

    <!--Full width header Start-->
    <div class="full-width-header">
        <!--Header Start-->
        <header id="rs-header" class="rs-header">
            <!-- Toolbar Area Start -->
            <div class="toolbar-area topbar-style1 hidden-md">
                <div class="container">
                    <div class="row rs-vertical-middle">
                        <div class="col-lg-12">
                            <div class="toolbar-contact">
                                <ul class="rs-contact-info">
                                    <li>
                                        <i class="fi fi-rr-envelope-plus"></i>
                                        <a href="mailto:{{getConstField('email')}}">{{getConstField('email')}}</a>
                                    </li>
                                    <li>
                                        <i class="fi fi-rr-phone-call"></i>
                                        <a href="tel:{{str_replace(' ', '', getConstField('phone'))}}">{{getConstField('phone')}}</a>
                                        <a href="tel:{{str_replace(' ', '', getConstField('phone2'))}}">{{getConstField('phone2')}}</a>
                                    </li>
                                    <li>
                                        <i class="fi-rr-map-marker-home"></i>
                                        {{getConstField('company_address')}}, {{getConstField('company_post_code')}} {{getConstField('company_city')}}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Toolbar Area End -->

            <!-- Menu Start -->
            <div class="menu-area menu-sticky">
                <div class="container">
                    <div class="row-table">
                        <div class="col-cell header-logo">
                            <div class="logo-area">
                                <a href="/">
                                    <img class="normal-logo" src="{{asset('image/logo/logo-dark.png')}}" alt="logo">
                                    <img class="sticky-logo" src="{{asset('image/logo/logo-dark.png')}}" alt="logo">
                                </a>
                            </div>
                        </div>
                        <div class="col-cell">
                            <div class="rs-menu-area">
                                <div class="main-menu">
                                    <nav class="rs-menu hidden-md">
                                        @include('default.nav_item.main', ['name' => 'main'])
                                    </nav>
                                </div> <!-- //.main-menu -->
                            </div>
                        </div>
                        <div class="col-cell">
                            <div class="expand-btn-inner">
                                <ul>
                                    <li class="humburger">
                                        <a id="nav-expander" class="nav-expander bar" href="#">
                                            <div class="bar">
                                                <span class="dot1"></span>
                                                <span class="dot2"></span>
                                                <span class="dot3"></span>
                                                <span class="dot4"></span>
                                                <span class="dot5"></span>
                                                <span class="dot6"></span>
                                                <span class="dot7"></span>
                                                <span class="dot8"></span>
                                                <span class="dot9"></span>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Menu End -->

            <!-- Canvas Mobile Menu start -->
            <nav class="right_menu_togle mobile-navbar-menu" id="mobile-navbar-menu">
                <div class="close-btn">
                    <a id="nav-close2" class="nav-close">
                        <div class="line">
                            <span class="line1"></span>
                            <span class="line2"></span>
                        </div>
                    </a>
                </div>
                @include('default.nav_item.main', ['name' => 'main'])
                <!-- //.nav-menu -->

                <!-- //.nav-menu -->
                <div class="canvas-contact">
                    <div class="address-area">
                        <div class="address-list">
                            <div class="info-icon">
                                <i class="fi fi-rr-map-marker-home"></i>
                            </div>
                            <div class="info-content">
                                <h4 class="title">Kontakt</h4>
                                <em>{{getConstField('company_address')}}, {{getConstField('company_post_code')}} {{getConstField('company_city')}}</em>
                            </div>
                        </div>
                        <div class="address-list">
                            <div class="info-icon">
                                <i class="fi fi-rr-envelope-plus"></i>
                            </div>
                            <div class="info-content">
                                <h4 class="title">Email</h4>
                                <em><a href="mailto:{{getConstField('email')}}">{{getConstField('email')}}</a></em>
                            </div>
                        </div>
                        <div class="address-list">
                            <div class="info-icon">
                                <i class="fi fi-rr-phone-call"></i>
                            </div>
                            <div class="info-content">
                                <h4 class="title">Telefon</h4>
                                <em><a href="tel:{{str_replace(' ', '', getConstField('phone'))}}">{{getConstField('phone')}}</a></em>
                                <em><a href="tel:{{str_replace(' ', '', getConstField('phone2'))}}">{{getConstField('phone2')}}</a></em>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
            <!-- Canvas Menu end -->
        </header>
        <!--Header End-->
    </div>
{{--@include('default._helpers.lang_nav')--}}

@yield('content')


</div>
<!-- Main content End -->

<!-- Footer Start -->
<footer id="rs-footer" class="rs-footer footer-main-home" style="background-image: url({{asset('images/footer-bg.jpg')}})">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 pr-20 md-pr-15 md-mb-20">
                    <div class="textwidget">
                        <p class="pb-20">{{getConstField('short_description_company')}}</p>
                    </div>
                </div>
                <div class="col-lg-4 md-mb-10">
                    <h3 class="footer-title">Dane kontaktowe</h3>
                    <ul class="address-widget">
                        <li>
                            <i class="fi fi-rr-map-marker-home"></i>
                            <div class="desc">
                                {{getConstField('company_address')}} <br> {{getConstField('company_post_code')}} {{getConstField('company_city')}}
                            </div>
                        </li>
                        <li>
                            <i class="fi fi-rr-phone-call"></i>
                            <div class="desc">
                                <a href="tel:{{str_replace(' ', '', getConstField('phone'))}}">{{getConstField('phone')}}</a><br>
                                <a href="tel:{{str_replace(' ', '', getConstField('phone2'))}}">{{getConstField('phone2')}}</a>
                            </div>
                        </li>
                        <li>
                            <i class="fi fi-rr-envelope-plus"></i>
                            <div class="desc">
                                <a href="mailto:{{getConstField('email')}}">{{getConstField('email')}}</a>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-4 md-mb-10">
                    <h3 class="footer-title">Dane firmy</h3>
                    <ul class="site-map">
                        <li style="color: #ffffff; margin-bottom: 15px">{{getConstField('company_name')}}</li>
                        <li style="color: #ffffff; margin-bottom: 15px">{{getConstField('company_address')}}</li>
                        <li style="color: #ffffff; margin-bottom: 15px">{{getConstField('company_post_code')}} {{getConstField('company_city')}}</li>
                        <li style="color: #ffffff; margin-bottom: 15px">NIP: {{getConstField('company_nip')}}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row y-middle">
                <div class="col-lg-12">
                    <div class="copyright text-lg-start text-center">
                        <p><?php echo date("Y") ?> &copy; Wszelkie prawa zastrzeżone. Strona stworzona przez: <a href="https://palmax.pl">Palmax</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer End -->

<!-- start scrollUp  -->
<div id="scrollUp">
    <i class="fa fa-angle-up"></i>
</div>

<!-- Search Modal End -->
<script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/simplelightbox/2.10.3/simple-lightbox.jquery.min.js" integrity="sha512-iJCzEG+s9LeaFYGzCbDInUbnF03KbE1QV1LM983AW5EHLxrWQTQaZvQfAQgLFgfgoyozb1fhzhe/0jjyZPYbmQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script><script src="{{asset('js/frontend.js')}}"></script>
{{--<script src="{{asset('js/main.min.js')}}"></script>--}}

<script src="{{asset('js/modernizr-2.8.3.min.js')}}"></script>
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/jquery.nav.js')}}"></script>
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('js/wow.min.js')}}"></script>
<script src="{{asset('js/skill.bars.jquery.js')}}"></script>
<script src="{{asset('js/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{asset('js/slick.min.js')}}"></script>
<script src="{{asset('js/waypoints.min.js')}}"></script>
<script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('js/jquery.counterup.min.js')}}"></script>
<script src="{{asset('js/contact.form.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
<script src="{{asset('js/custom.js')}}"></script>


@stack('scripts.body.bottom')
</body>
</html>
