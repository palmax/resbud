@extends('default.layout')
@section('content')
    @include('default.subheader', ['pageName' => $page->name])
<div class="pt-120 pb-120 md-pt-80 md-pb-75">
    <div class="container">
        <div class="sec-title">
            <h2 class="title title4 pb-20" style="color: #010d14">
                Poniżej przedstawiamy naszą ofertę:
            </h2>
        </div>
        <ul class="check-lists">
            @foreach($items as $item)
                <li class="list-item">
                    <span class="icon-list-icon">
                        <i class="fa fa-check-circle"></i>
                    </span>
                    <span class="list-text">{{$item->title}}</span>
                </li>
            @endforeach
        </ul>
    </div>
</div>
    <div class="rs-cta bg4 pt-120 pb-115 md-pt-80 md-pb-75 bg_dark_opacity" style="background-image: url({{asset('images/about-single-bg.jpg')}})">
        <div class="container">
            <div class="call-action">
                <div class="sec-title text-center">
                    <span class="sub-text yellow-color">
                        <img src="{{asset('images/shape-1.png')}}" alt="Images">
                        Kontakt
                    </span>
                    <h2 class="title white-color pb-20">
                        Zapraszamy do współpracy
                    </h2>
                    <p class="desc desc3 pb-35">@if(isset($fields->contact_description))
                            {!! $fields->contact_description !!}
                        @endif</p>
                    <div class="btn-part">
                        <a class="readon more contact" href="{{route('contact.show')}}">Skontaktuj się z nami</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
