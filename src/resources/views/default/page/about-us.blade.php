@extends('default.layout')
@section('content')
    @include('default.subheader', ['pageName' => $page->name])

<div class="rs-about about-style1 bg1 pt-120 pb-120 md-pt-80 md-pb-75">
    <div class="container">
        <div class="row y-middle">
            <div class="col-lg-6 md-mb-50">
                <div class="images-part">
                    <img src="{{asset('images/about_us_page.jpg')}}" style="    height: 551px;
    object-fit: cover;" alt="O firmie">
                </div>
            </div>
            <div class="col-lg-6 pl-50 md-pl-15">
                <div class="sec-title">
                            <span class="sub-text">
                                <img src="{{asset('images/shape-1.png')}}" alt="Images">
                                O nas
                            </span>
                    <h2 class="title pb-20">
                        Kilka słów o nas
                    </h2>
                    <p class="desc">@if(isset($fields->about_us_description_about))
                            {!! $fields->about_us_description_about !!}
                        @endif</p>
                    <div class="mt-25">
                        <ul class="check-lists">
                            <li class="list-item">
                                            <span class="icon-list-icon">
                                                <i class="fa fa-check-circle"></i>
                                            </span>
                                <span class="list-text">@if(isset($fields->experience_description_about))
                                        {!! $fields->experience_description_about !!}
                                    @endif</span>
                            </li>
                            <li class="list-item">
                                            <span class="icon-list-icon">
                                                <i class="fa fa-check-circle"></i>
                                            </span>
                                <span class="list-text">@if(isset($fields->service_description_about))
                                        {!! $fields->service_description_about !!}
                                    @endif</span>
                            </li>
                            <li class="list-item">
                                            <span class="icon-list-icon">
                                                <i class="fa fa-check-circle"></i>
                                            </span>
                                <span class="list-text">@if(isset($fields->mission_and_value_description_about))
                                        {!! $fields->mission_and_value_description_about !!}
                                    @endif</span>
                            </li>
                        </ul>
                    </div>
                    <div class="row mt-25 md-mt-25">
                        <div  class="col-lg-12 sm-mb-30">
                            <div class="btn-part">
                                <a class="readon more know" href="{{route('gallery.show')}}">Zobacz grafikę</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="rs-cta bg4 pt-120 pb-115 md-pt-80 md-pb-75 bg_dark_opacity" style="background-image: url({{asset('images/about-single-bg.jpg')}})">
    <div class="container">
        <div class="call-action">
            <div class="sec-title text-center">
                    <span class="sub-text yellow-color">
                        <img src="{{asset('images/shape-1.png')}}" alt="Images">
                        Kontakt
                    </span>
                <h2 class="title white-color pb-20">
                    Zapraszamy do współpracy
                </h2>
                <p class="desc desc3 pb-35">@if(isset($fields->contact_description))
                        {!! $fields->contact_description !!}
                    @endif</p>
                <div class="btn-part">
                    <a class="readon more contact" href="{{route('contact.show')}}">Skontaktuj się z nami</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
