@extends('default.layout')
@section('content')
    @include('default.subheader', ['pageName' => $page->name])

    <div class="rs-contact contact-style6 pb-120 md-pb-80">
        <div class="row no-gutters">
            <div class="col-lg-4">
                <div class="contact-map">
                    {!! getConstField('google_map_iframe') !!}
                </div>
            </div>
            <div class="col-lg-4">
                <div class="contact-section">
                    <div class="contact-wrap">
                        <div class="sec-title mb-60">
                            <h2 class="title title4 ser-style4">Napisz do nas</h2>
                        </div>
                        <div id="form-messages"></div>
                        @include('default.form.contact_form')
                    </div>
                </div>
            </div>
            <div class="col-lg-4 contact-project" style="background-image: url({{asset('images/contact_image.png')}})"></div>
        </div>
        <!-- Contact Icons Section End -->
        <div class="rs-contact pt-120 md-pt-80">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6 md-mb-30">
                        <div class="contact-box">
                            <div class="contact-icon">
                                <img src="{{asset('image/icon/1.png')}}" alt="images">
                            </div>
                            <div class="content-text">
                                <h4 class="title" style="color: #fff">Adres</h4>
                                <p class="services-txt">{{getConstField('company_address')}}, {{getConstField('company_post_code')}} {{getConstField('company_city')}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 sm-mb-30">
                        <div class="contact-box">
                            <div class="contact-icon">
                                <img src="{{asset('image/icon/2.png')}}" alt="images">
                            </div>
                            <div class="content-text">
                                <h4 class="title" style="color: #fff">Email</h4>
                                <span><a href="mailto:{{getConstField('email')}}">{{getConstField('email')}}</a></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="contact-box">
                            <div class="contact-icon">
                                <img src="{{asset('image/icon/3.png')}}" alt="images">
                            </div>
                            <div class="content-text">
                                <h4 class="title" style="color: #fff">Telefony</h4>
                                <span><a href="tel:{{str_replace(' ', '', getConstField('phone'))}}">{{getConstField('phone')}}</a></span><br>
                                <span><a href="tel:{{str_replace(' ', '', getConstField('phone2'))}}">{{getConstField('phone2')}}</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Contact Icons Section End -->
    </div>
@endsection
