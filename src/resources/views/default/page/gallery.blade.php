@extends('default.layout')
@section('content')
    @include('default.subheader', ['pageName' => $page->name])
    <div class="pt-120 pb-120 md-pt-80 md-pb-75">
        <div class="container">
            <div class="gallery">
                <div class="container">
                    <div class="row">
                        @foreach($page->gallery->items as $item)
                            <div class="col-lg-4 magnific-img">
                                <a class="image-popup-vertical-fit" href="{{renderImage($item->url, 1920, 1080, 'resize')}}">
                                    <img style="width: 100%" src="{{renderImage($item->url, 600, 600, 'fit')}}" alt="">
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('scripts.body.bottom')
        <script>
            var lightbox = $('.gallery a').simpleLightbox({});
        </script>
    @endpush
@endsection
