@extends('default.layout')
@section('content')
{{--{!! getPhoneLink('phone', 'phone', '<b>icon</b> ') !!}--}}
{{--{!! getEmailLink('email', 'email',  '<b>icon</b> ') !!}--}}
{{--    <span style="display: block">{!! getAddressString() !!}</span>--}}
{{--    <span style="display: block">{!! getFooterCreator() !!}</span>--}}

@include('default.rotator.base', ['id_rotator' => $fields->rotator, 'type' => 'main'])
<!-- About Start -->
<div class="rs-about about-style1 bg1 pt-120 pb-120 md-pt-80 md-pb-80" style="background-image: url({{asset('images/about-bg.jpg')}})">
    <div class="container">
        <div class="row y-middle">
            <div class="col-lg-6 md-mb-50">
                <div class="images-part">
                    <img src="{{asset('images/about_us.jpg')}}" style="    height: 551px;
    object-fit: cover;" alt="O nas">
                </div>
            </div>
            <div class="col-lg-6 pl-50 md-pl-15">
                <div class="sec-title">
							<span class="sub-text">
								<img src="{{asset('images/shape-1.png')}}" alt="Images">
								O nas
							</span>
                    <h2 class="title pb-20">
                        Kilka słów o nas
                    </h2>
                    <p class="desc">@if(isset($fields->about_us_description))
                            {!! $fields->about_us_description !!}
                        @endif</p>
                    <div class="row mt-35 md-mt-25">
                        <div class="col-lg-12 col-md-12 sm-mb-30">
                            <div class="btn-part">
                                <a class="readon more know" href="{{route('about-us.show')}}">Zobacz więcej</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- About End -->

<div class="rs-cta bg4 pt-120 pb-115 md-pt-80 md-pb-75 bg_dark_opacity" style="position: relative;background-image: url({{asset('images/about-single-bg2.jpg')}})">
    <div class="container">
        <div class="call-action">
            <div class="sec-title text-center">
						<span class="sub-text yellow-color">
							<img src="{{asset('images/shape-1.png')}}" alt="Images">
							Nasza wizja
						</span>
                <h2 class="title white-color pb-20">
                    Oto nasza wizja
                </h2>
                <p class="desc desc3 pb-35">@if(isset($fields->our_vision_description))
                        {!! $fields->our_vision_description !!}
                    @endif</p>
                <div class="btn-part">
                        <a class="readon more contact" href="{{route('offer.index')}}">Zobacz ofertę</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Services Section Start -->
<div class="rs-services services-main-home gray-bg pt-120 pb-120 md-pt-80 md-pb-80">
    <div class="container">
        <div class="sec-title text-center mb-55 md-mb-35">
					<span class="sub-text">
						<img src="{{asset('images/shape-1.png')}}" alt="Images">
						Nasze zalety
					</span>
            <h2 class="title">Oto nasze zalety</h2>
            <p class="desc pt-25">@if(isset($fields->our_advantages_description))
                    {!! $fields->our_advantages_description !!}
                @endif</p>
        </div>
        <div class="row" style="justify-content: center;">
            @include('default.article.home')
        </div>
    </div>
</div>
<!-- Services Section End -->

<!-- Call To Action Choose Start -->
<div class="rs-cta bg4 pt-120 pb-115 md-pt-80 md-pb-75 bg_dark_opacity" style="position: relative; background-image: url({{asset('images/about-single-bg.jpg')}})">
    <div class="container">
        <div class="call-action">
            <div class="sec-title text-center">
						<span class="sub-text yellow-color">
							<img src="{{asset('images/shape-1.png')}}" alt="Images">
							Kontakt
						</span>
                <h2 class="title white-color pb-20">
                    Zapraszamy do współpracy
                </h2>
                <p class="desc desc3 pb-35">@if(isset($fields->contact_description))
                        {!! $fields->contact_description !!}
                    @endif</p>
                <div class="btn-part">
                    <a class="readon more contact" href="{{route('contact.show')}}">Skontaktuj się z nami</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
