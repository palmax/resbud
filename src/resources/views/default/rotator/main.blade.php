<div class="rs-slider slider-style1 ">
    <div class="slider slider-slide-1" id="rotator{{$rotator->id}}">
        @foreach($rotator->gallery->items as $item)
            <div class="slider-item" style="background-image: url('{{asset('image/slider/h1-1.png')}}')">
                <div class="container custom13">
                    <div class="slider-inner">
                        <div class="content-part row">
                            <div class="col-6">
                                	<span class="sl-subtitle wow fadeInDown">
									<img class="sl-icons" src="{{asset('image/slider/icons3.png')}}"
                                         alt="Images">{{$item->name}}</span>
                                <h1 class="sl-title wow fadeInLeft">{!! $item->text !!}</h1>
                                <div class="slider-bottom wow fadeinup">
                                    <a class="readon more" href="{{route('about-us.show')}}">Zobacz więcej</a>
                                </div>
                            </div>
                            <div class="col-6">
                                <img style="width: 576px; height: 385px; object-fit: cover;" src="{{renderImage($item->url, 1920, 700, `fit`)}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="animation-wrap">
                    <div class="animate-style one">
                        <img class="wow spine" src="{{asset('image/slider/icons1.png')}}" alt="Images">
                    </div>
                    <div class="animate-style two">
                        <img class="wow spine" src="{{asset('image/slider/icons2.png')}}" alt="Images">
                    </div>
                    <div class="animate-style three">
                        <img class="wow spine" src="{{asset('image/slider/icons2.png')}}" alt="Images">
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@push('scripts.body.bottom')
    <script>
        $('#rotator{{$rotator->id}}').slick({
            autoplay: true,
            infinite: false,
            centerMode: false,
            autoplaySpeed: {{$rotator->time ?? 3000}},
            speed: {{$rotator->speed ?? 500}},
            arrows: {{$rotator->arrows ? 'true' : 'false'}},
            dots: {{$rotator->pager ? 'true' : 'false'}},
            loop: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                }
            ]
        });
    </script>
@endpush
