<div class="rs-breadcrumbs img1" style="background: url({{asset('images/inr-1.jpg')}})">
    <div class="container">
        <div class="breadcrumbs-inner">
            @if(isset($pageName))
            <h1 class="page-title">
                {!! $pageName !!}
            </h1>
            @endif
            <ul class="breadcrumbs-area">
                <li title="Go to konstruk">
                    <a class="active" href="/">Strona główna</a>
                </li>
                @if(isset($pageName))
                <li>{!! $pageName !!}</li>
                @endif
            </ul>
        </div>
    </div>
</div>
